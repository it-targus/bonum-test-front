import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import {SheduleComponent} from './shedule/shedule/shedule.component';

const routes: Routes = [
  // {
  //   path: '',
  //   component: SheduleComponent,
  // }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
