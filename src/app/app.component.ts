import {Component, OnInit} from '@angular/core';
import {SheduleService} from './shedule/shedule.service';
import {ISelectEvent, IShedule, ISheduledTask} from './shedule';
import {Observable} from 'rxjs';
import {WsService} from './ws/ws.service';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  shedule: IShedule;
  blocked$: Observable<Array<ISheduledTask>>;

  constructor(
      private sheduleService: SheduleService,
      private ws: WsService,
  ) {
  }

  onPeriodSelected(event: ISelectEvent) {
    this.ws.send('block', {
      date: event.date.getTime(),
      selectedIntervals: event.selectedIntervals.map(item => {
        return {
          startTime: item.startTime.getTime(),
          finishTime: item.finishTime.getTime(),
        };
      }),
    });
  }

  onPeriodConfirmed(event: ISelectEvent) {
    // @ToDo: Implement request to backend
  }

  ngOnInit() {
    this.sheduleService.getSheduleCollection().subscribe(tasks => {
      this.shedule = this.sheduleService.convertShedules(tasks);
    });
    this.blocked$ = this.ws.getObservableForEvent('block').pipe(
        map(data => {
          let collection = [];
          const id = this.ws.getId();
          for (const idx in data) {
            if (idx === id) {
              continue;
            }
            collection = collection.concat(data[idx]);
          }

          return collection.map(item => {
            return {
              startTime: new Date(item.startTime),
              finishTime: new Date(item.finishTime),
            };
          });
        })
    );
  }
}
