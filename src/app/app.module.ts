import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {SheduleModule} from './shedule';
import {WsModule} from './ws/ws.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule.withServerTransition({appId: 'bonum-test-front'}),
    AppRoutingModule,
    SheduleModule,
    WsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
