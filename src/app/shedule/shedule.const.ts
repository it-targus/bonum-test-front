export const DEFAULT_WORKING_HOURS = {
  start: {
    hour: 8,
    minutes: 0,
  },
  finish: {
    hour: 17,
    minutes: 0,
  }
};

export const DEFAULT_PERIOD = 15;

export const DEFAULT_DISABLE_DAYS = [0, 6];
