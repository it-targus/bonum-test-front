export type DisableDays = Array<number>;

export type DisableDates = Array<Date>;

export interface ITime {
  hour: number;
  minutes: number;
}

export interface IWorkingHours {
  start: ITime;
  finish: ITime;
}

export interface ISheduledTask {
  startTime: Date;
  finishTime: Date;
  self?: boolean;
}

export interface IMonthShedule {
    [index: number]: Array<ISheduledTask>;
}

export interface IYearShedule {
    [index: number]: IMonthShedule;
}

export interface IShedule {
    [index: number]: IYearShedule;
}

export interface ISelectEvent {
  date: Date;
  selectedIntervals: Array<ISheduledTask>;
}
