import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {SheduleComponent} from './shedule/shedule.component';
import {CalendarModule} from 'primeng/calendar';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ConfirmDialogModule} from 'primeng/components/confirmdialog/confirmdialog';
import {ConfirmationService} from 'primeng/components/common/confirmationservice';
import {SheduleService} from './shedule.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    BrowserAnimationsModule,
    CalendarModule,
    ConfirmDialogModule,
  ],
  declarations: [
    SheduleComponent,
  ],
  providers: [
    ConfirmationService,
    SheduleService,
  ],
  exports: [
    SheduleComponent,
  ]
})
export class SheduleModule {
}
