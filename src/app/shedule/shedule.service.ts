import { Injectable } from '@angular/core';
import {BehaviorSubject, interval, Observable, of, Subject} from 'rxjs';
import {IShedule, ISheduledTask} from './shedule.model';

const mockTasks: Array<ISheduledTask> = [
  {
    startTime: new Date('02/05/2019 13:15:00'),
    finishTime: new Date('02/05/2019 13:45:00'),
  },
  {
    startTime: new Date('02/05/2019 15:30:00'),
    finishTime: new Date('02/05/2019 15:45:00'),
  }
];

const mockBlockedIntervals: Array<ISheduledTask> = [
  {
    startTime: new Date('01/28/2019 10:15:00'),
    finishTime: new Date('01/28/2019 10:45:00'),
  },
  {
    startTime: new Date('01/28/2019 11:30:00'),
    finishTime: new Date('01/28/2019 11:45:00'),
  }
];

@Injectable({
  providedIn: 'root'
})
export class SheduleService {

  private blocked$ = new BehaviorSubject<Array<ISheduledTask>>([]);

  constructor() {
  }

  getSheduleCollection(user?): Observable<Array<ISheduledTask>> {
    return of(mockTasks);
  }

  convertShedules(tasks: Array<ISheduledTask>): IShedule {
    const shedule = {};
    tasks.forEach(task => {
      const year = task.startTime.getFullYear();
      if (!shedule.hasOwnProperty(year)) {
        shedule[year] = {};
      }
      const month = task.startTime.getMonth();
      if (!shedule[year].hasOwnProperty(month)) {
        shedule[year][month] = {};
      }
      const day = task.startTime.getDate();
      if (!shedule[year][month].hasOwnProperty(day)) {
        shedule[year][month][day] = [];
      }
      shedule[year][month][day].push(task);
    });

    return shedule;
  }
}
