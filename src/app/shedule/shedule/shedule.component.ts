import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {DisableDates, DisableDays, ISelectEvent, IShedule, ISheduledTask, ITime, IWorkingHours} from '../shedule.model';
import {DEFAULT_DISABLE_DAYS, DEFAULT_PERIOD, DEFAULT_WORKING_HOURS} from '../shedule.const';
import {ConfirmationService} from 'primeng/components/common/confirmationservice';

const MAX_PERIOD_QUANTITY = 3;

enum IntervalStatus {
  Available = 'available',
  Blocked = 'blocked',
  Reserved = 'reserved',
  Selected = 'selected',
}

type Period = 5 | 10 | 15 | 20 | 30 | 60;

interface ITaskInterval {
  time: ITime;
  status: IntervalStatus;
  tmpBlocked?: boolean;
}

type IHourLine = Array<ITaskInterval>;

@Component({
  selector: 'app-shedule',
  templateUrl: './shedule.component.html',
  styleUrls: ['./shedule.component.scss'],
  providers: [ConfirmationService]
})
export class SheduleComponent implements OnInit, OnChanges {

  @Input('working-hours') workingHours: IWorkingHours = DEFAULT_WORKING_HOURS;
  @Input() period: Period = DEFAULT_PERIOD; // in minutes

  @Input('disable-days') disableDays: DisableDays = DEFAULT_DISABLE_DAYS;
  @Input('disable-dates') disableDates: DisableDates = [];

  @Input() shedule: IShedule = {};
  @Input() blocked: Array<ISheduledTask> = [];

  @Output() onSelect = new EventEmitter<ISelectEvent>();
  @Output('onConfirm') onConfirmEmiter = new EventEmitter<ISelectEvent>();

  public validateErrors: Array<string> = [];

  public minDate: Date;
  // Make enum visible in view template
  public IntervalStatus = IntervalStatus;

  private prevSelectedDate: Date;
  public selectedDate: Date;
  public hourLines: Array<IHourLine> = [];

  public selectedIntrevals: Array<ISheduledTask> = [];

  constructor(
      private confirmator: ConfirmationService,
  ) { }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.hasOwnProperty('blocked')) {
      this.processBlocked();
    }
  }

  onDateChanged(date: Date) {
    if (this.selectedIntrevals.length > 0) {
      this.confirmator.confirm({
        message: 'All selections will be lost. Are you sure?',
        accept: () => {
          this.selectedIntrevals = [];
          this.rebuildHourLinesForDate();
          this.setTmpBlock();
          this.emitSelection(new Date(this.prevSelectedDate));
          this.prevSelectedDate = new Date(date);
        },
        reject: () => {
          this.selectedDate = new Date(this.prevSelectedDate);
        }
      });
    } else {
      this.rebuildHourLinesForDate();
      this.prevSelectedDate = new Date(date);
    }
  }

  onClear() {
    this.selectedIntrevals = [];
    this.hourLines.forEach(line => line.forEach(cell => {
      if (cell.status === IntervalStatus.Selected) {
        cell.status = IntervalStatus.Available;
      }
    }));
    this.setTmpBlock();
    this.emitSelection();
  }

  onConfirm() {
    const event: ISelectEvent = {
      date: new Date(this.selectedDate),
      selectedIntervals: this.selectedIntrevals
    };
    this.onConfirmEmiter.emit(event);
  }

  emitSelection(date?: Date) {
    const event: ISelectEvent = {
      date: new Date(date || this.selectedDate),
      selectedIntervals: this.selectedIntrevals
    };
    this.onSelect.emit(event);
  }

  public toggle(lineIdx: number, colIdx: number) {
    const cell = this.hourLines[lineIdx][colIdx];
    const hour = cell.time.hour;
    const minutes = cell.time.minutes;
    const isReserved = this.checkReservedIntervals(hour, minutes);
    const isBlocked = this.checkBlockedIntervals(hour, minutes);
    if (isReserved || isBlocked) {
      return;
    }
    const start = new Date(this.selectedDate);
    const finish = new Date(this.selectedDate);
    start.setHours(hour, minutes, 0, 0);
    finish.setHours(hour, minutes + this.period, 0, 0);
    const selectedIdx = this.selectedIntrevals.findIndex(item => item.startTime.getTime() === start.getTime());
    const prevCell = this.getNearCell(lineIdx, colIdx, 'backward');
    const nextCell = this.getNearCell(lineIdx, colIdx, 'forward');

    // Avoid unselect middle cell in selected range
    if (
        selectedIdx !== -1
        && prevCell
        && prevCell.status === IntervalStatus.Selected
        && nextCell
        && nextCell.status === IntervalStatus.Selected
    ) {
      return;
    }

    // Selected interval should be inseparable
    if (
        this.selectedIntrevals.length > 0
        && selectedIdx === -1
        && (!prevCell || prevCell.status !== IntervalStatus.Selected)
        && (!nextCell || nextCell.status !== IntervalStatus.Selected)
    ) {
      return;
    }

    // Limit selected intevals
    if (selectedIdx === -1 && this.selectedIntrevals.length >= MAX_PERIOD_QUANTITY) {
      return;
    }

    cell.status = selectedIdx !== -1 ? IntervalStatus.Available : IntervalStatus.Selected;
    if (selectedIdx === -1) {
      this.selectedIntrevals.push({
        startTime: start,
        finishTime: finish,
      });
    } else {
      this.selectedIntrevals.splice(selectedIdx, 1);
    }

    this.setTmpBlock();
    this.emitSelection();
  }

  private setTmpBlock() {
    const isSelected = this.selectedIntrevals.length > 0;
    let prevCell: ITaskInterval;
    this.hourLines.forEach((line, i) => line.forEach((cell, j) => {
      if (isSelected) {
        if (cell.status === IntervalStatus.Selected) {
          if (prevCell && prevCell.status !== IntervalStatus.Selected) {
            prevCell.tmpBlocked = this.selectedIntrevals.length >= MAX_PERIOD_QUANTITY;
          }
          cell.tmpBlocked = false;
        } else if (prevCell && prevCell.status === IntervalStatus.Selected) {
          cell.tmpBlocked = this.selectedIntrevals.length >= MAX_PERIOD_QUANTITY;
        } else {
          cell.tmpBlocked = true;
        }
      } else {
        cell.tmpBlocked = false;
      }
      prevCell = cell;
    }));
  }

  private processBlocked() {
    const start = new Date(this.selectedDate);
    const finish = new Date(this.selectedDate);
    this.hourLines.forEach((line) => line.forEach((cell) => {
      start.setHours(cell.time.hour, cell.time.minutes, 0, 0);
      finish.setHours(cell.time.hour, cell.time.minutes + this.period, 0, 0);
      let intersected = false;
      for (let i = 0; i < this.blocked.length; i++) {
        if (this.blocked[i].startTime.getTime() < finish.getTime() && this.blocked[i].finishTime.getTime() > start.getTime()) {
          intersected = true;
          break;
        }
      }
      // cell.status = intersected ? IntervalStatus.Blocked : IntervalStatus.Available;
      if (intersected) {
        cell.status = IntervalStatus.Blocked;
      } else if (cell.status === IntervalStatus.Blocked) {
        cell.status = IntervalStatus.Available;
      }
    }));
  }

  private rebuildHourLinesForDate() {
    const hourLines: Array<IHourLine> = [];
    for (let i = this.workingHours.start.hour; i <= this.workingHours.finish.hour; i++) {
      const startMinutes = i === this.workingHours.start.hour ? this.workingHours.start.minutes : 0;
      const finishMinutes = i === this.workingHours.finish.hour ? this.workingHours.finish.minutes : 60;
      const hourLine: IHourLine = [];
      for (let j = startMinutes; j + this.period <= finishMinutes; j += this.period) {
        const isReserved = this.checkReservedIntervals(i, j);
        const isBlocked = this.checkBlockedIntervals(i, j);
        const isSelected = this.checkSelectedIntervals(i, j);
        let status: IntervalStatus;
        if (isReserved) {
          status = IntervalStatus.Reserved;
        } else if (isBlocked) {
          status = IntervalStatus.Blocked;
        } else if (isSelected) {
          status = IntervalStatus.Selected;
        } else {
          status = IntervalStatus.Available;
        }
        hourLine.push({
          time: {
            hour: i,
            minutes: j,
          },
          status,
        });
      }
      if (hourLine.length > 0) {
        hourLines.push(hourLine);
      }
    }
    this.hourLines = hourLines;
  }

  private checkReservedIntervals(hour: number, minutes: number) {
    let thisDateTasks: Array<ISheduledTask> = [];
    if (
        this.shedule[this.selectedDate.getFullYear()]
        && this.shedule[this.selectedDate.getFullYear()][this.selectedDate.getMonth()]
        && this.shedule[this.selectedDate.getFullYear()][this.selectedDate.getMonth()][this.selectedDate.getDate()]
    ) {
      thisDateTasks = this.shedule[this.selectedDate.getFullYear()][this.selectedDate.getMonth()][this.selectedDate.getDate()];
    }

    return this.checkIntersectInterval(thisDateTasks, hour, minutes);
  }

  private checkBlockedIntervals(hour: number, minutes: number) {
    return this.checkIntersectInterval(this.blocked || [], hour, minutes);
  }

  private checkSelectedIntervals(hour: number, minutes: number) {
    return this.checkIntersectInterval(this.selectedIntrevals, hour, minutes);
  }

  private checkIntersectInterval(tasks: Array<ISheduledTask>, hour: number, minutes: number): boolean {
    const minDate = new Date(this.selectedDate);
    const maxDate = new Date(this.selectedDate);
    minDate.setHours(hour, minutes, 0, 0);
    maxDate.setHours(hour, minutes + this.period, 0, 0);
    return tasks.filter(
        item => item.startTime.getTime() < maxDate.getTime() && item.finishTime.getTime() > minDate.getTime()
    ).length > 0;
  }

  private validateInputData() {
    if ([5, 10, 15, 20, 30, 60].indexOf(this.period)) {
      this.validateErrors.push('Invalid period. 5, 10, 15, 20, 30 or 60 allowed');
    }
    if (
      this.workingHours.start.hour < 0
      || this.workingHours.start.hour > 23
      || this.workingHours.finish.hour < 0
      || this.workingHours.finish.hour > 23
    ) {
      this.validateErrors.push('Hours should be in 0-23 range');
    }
    if (
      this.workingHours.start.minutes < 0
      || this.workingHours.start.minutes > 49
      || this.workingHours.finish.minutes < 0
      || this.workingHours.finish.minutes > 49
    ) {
      this.validateErrors.push('Minutes should be in 0-23 range');
    }
    if (
      this.workingHours.start.hour > this.workingHours.finish.hour
      || (
        this.workingHours.start.hour === this.workingHours.finish.hour
        && this.workingHours.start.minutes >= this.workingHours.finish.minutes
      )
    ) {
      this.validateErrors.push('Invalid working hours range');
    }
  }

  // Avoid to select forbidden dates
  getNearestDate(date: Date, direction: 'forward' | 'backward' = 'forward'): Date {
      const newDate = new Date(date);
      newDate.setHours(0, 0, 0, 0);
      const disabledTimestamps = this.disableDates.map(item => {
          const d = new Date(item);
          d.setHours(0, 0, 0, 0);
          return d.getTime();
      });
      const delta = direction === 'forward' ? 1 : -1;
      while (
          this.disableDays.indexOf(newDate.getDay()) !== -1
          || newDate.getTime() < this.minDate.getTime()
          || disabledTimestamps.indexOf(newDate.getTime()) !== -1
      ) {
          newDate.setDate(newDate.getDate() + delta);
      }

      return newDate;
  }

  getNearCell(lineIdx: number, colIdx: number, direction: 'forward' | 'backward'): ITaskInterval {
    const delta = direction === 'forward' ? 1 : -1;
    let newLineIdx = lineIdx;
    let newColIdx = colIdx + delta;
    if (newColIdx < 0) {
      newLineIdx--;
      if (newLineIdx < 0) {
        return null;
      }
      newColIdx = this.hourLines[newLineIdx].length - 1;
    } else if (newColIdx >= this.hourLines[newLineIdx].length) {
      newLineIdx++;
      if (newLineIdx >= this.hourLines.length) {
        return null;
      }
      newColIdx = 0;
    }

    return this.hourLines[newLineIdx][newColIdx];
  }

  ngOnInit() {
    this.validateInputData();
    this.minDate = new Date();
    this.minDate.setHours(0, 0, 0, 0);
    this.selectedDate = this.getNearestDate(new Date());
    this.prevSelectedDate = new Date(this.selectedDate);
    this.rebuildHourLinesForDate();
  }

}
