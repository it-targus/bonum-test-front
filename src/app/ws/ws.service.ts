import {Inject, Injectable, PLATFORM_ID} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {filter, map} from 'rxjs/operators';
import {isPlatformServer} from '@angular/common';

export interface IWsMessage<T = any> {
  event: string;
  data: any;
}

@Injectable({
  providedIn: 'root'
})
export class WsService {

  private messages$ = new Subject<IWsMessage>();
  private ws: WebSocket;

  private id: string;

  constructor(
    @Inject(PLATFORM_ID) private platformId: any,
  ) {
    if (!isPlatformServer(this.platformId)) {
      const ws = new WebSocket('ws://localhost:3005');
      ws.onopen = () => {
        this.ws = ws;
        this.ws.onmessage = (message) => {
          this.messages$.next(JSON.parse(message.data));
        };
        this.messages$.pipe(
            filter(message => message.event === 'init'),
        ).subscribe(message => {
          this.id = message.data;
        });
      };
    }
  }

  getId(): string {
    return this.id;
  }

  getObservableForEvent<T = any>(eventName: string): Observable<T> {
    return this.messages$.pipe(
        filter(message => message.event === eventName),
        map(message => message.data),
    );
  }

  send(eventName, data) {
    if (this.ws) {
      this.ws.send(JSON.stringify({event: eventName, data}));
    }
  }
}
